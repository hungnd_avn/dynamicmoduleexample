package com.theme;


import com.example.hungnd.dynamicmoduleexample.Util.LogUtils;

public class DynamicUtils {
    public static int getDrawableResourceId(String fileName) {
        try {
            int resId = R.drawable.class.getField(fileName).getInt(null);
            LogUtils.logD("DynamicUtils", "getDrawableResourceId: ", fileName, resId);
            return resId;
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
