package com.example.hungnd.dynamicmoduleexample.data.entity;

public class ThemeObj {
    public boolean isDynamicTheme;
    public String category;
    public int position;
    public String fileName;

    public ThemeObj(String category, int i, boolean isDynamicTheme) {
        this.isDynamicTheme = isDynamicTheme;
        this.category = category;
        position = i;
        fileName = category + String.valueOf(i);
    }

    public ThemeObj(ThemeObj theme){
        this.isDynamicTheme = theme.isDynamicTheme;
        this.category = theme.category;
        this.fileName = theme.fileName;
        this.position = theme.position;
    }
}
