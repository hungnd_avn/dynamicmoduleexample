package com.example.hungnd.dynamicmoduleexample.ui.changetheme;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.hungnd.dynamicmoduleexample.R;
import com.example.hungnd.dynamicmoduleexample.Util.LogUtils;
import com.example.hungnd.dynamicmoduleexample.Util.ToastUtils;
import com.example.hungnd.dynamicmoduleexample.Util.Utils;
import com.example.hungnd.dynamicmoduleexample.data.entity.ThemeObj;
import com.example.hungnd.dynamicmoduleexample.ui.BaseActivity;
import com.example.hungnd.dynamicmoduleexample.ui.changetheme.adapter.ThemeAdapter;
import com.google.android.play.core.splitinstall.SplitInstallHelper;
import com.google.android.play.core.splitinstall.SplitInstallManager;
import com.google.android.play.core.splitinstall.SplitInstallManagerFactory;
import com.google.android.play.core.splitinstall.SplitInstallRequest;
import com.google.android.play.core.splitinstall.SplitInstallSessionState;
import com.google.android.play.core.splitinstall.SplitInstallStateUpdatedListener;
import com.google.android.play.core.splitinstall.model.SplitInstallSessionStatus;
import com.google.android.play.core.tasks.OnFailureListener;
import com.google.android.play.core.tasks.OnSuccessListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ChangeThemeActivity extends BaseActivity {

    public static boolean requestToChangeTheme = false;
    private SplitInstallManager splitInstallManager;
    private int mySessionId;
    public static boolean isThemeModuleHasJustBeenDownloaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_theme);
        ButterKnife.bind(this);

        initData();
    }

    private void initData() {
        splitInstallManager = SplitInstallManagerFactory.create(getApplicationContext());
        Fragment fragment;
        //Check to show corresponding Fragment
        //If the dynamic module has not been downloaded yet, show DefaultThemeFragment
        if (splitInstallManager.getInstalledModules().contains("theme")) {
            fragment = new DynamicThemeFragment();
        } else {
            fragment = new DefaultThemeFragment();
        }
        addRootFragment(R.id.fl_fragment_container, fragment);
    }

    public void addRootFragment(int containerId, Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .add(containerId, fragment)
                .commit();
    }

    public void startDownloadThemeProcess() {
        downloadTheme();
    }

    public void downloadTheme() {
        LogUtils.logD("ChangeThemeActivity", "onclick downloadTheme: ");
        Single.create(emitter -> {
            SplitInstallRequest request =
                    SplitInstallRequest
                            .newBuilder()
                            .addModule("theme")
                            .build();
            splitInstallManager.startInstall(request).addOnSuccessListener(new OnSuccessListener<Integer>() {
                @Override
                public void onSuccess(Integer sessionId) {
                    mySessionId = sessionId;
                    ToastUtils.show("start installing");
                    LogUtils.logD("ChangeThemeActivity", "onSuccess: ");
                }
            })
                    .addOnFailureListener(e -> onDownloadThemFailed(e.getMessage()));
            splitInstallManager.registerListener(new SplitInstallStateUpdatedListener() {
                @Override
                public void onStateUpdate(SplitInstallSessionState splitInstallSessionState) {
                    int sessionId = splitInstallSessionState.sessionId();
                    LogUtils.logD("ChangeThemeActivity", "onStateUpdate: ", sessionId);
                    if (sessionId == mySessionId) {
                        int status = splitInstallSessionState.status();
                        LogUtils.logD("ChangeThemeActivity", "downloadTheme: Status", status);
                        switch (status) {
                            case SplitInstallSessionStatus.INSTALLED:
                                ChangeThemeActivity.this.onDownloadThemSuccess();
                                break;
                            case SplitInstallSessionStatus.FAILED:
                                ChangeThemeActivity.this.onDownloadThemFailed("error");
                                break;
                            case SplitInstallSessionStatus.REQUIRES_USER_CONFIRMATION:
                                try {
                                    ChangeThemeActivity.this.startIntentSender(splitInstallSessionState.resolutionIntent().getIntentSender(), null, 0, 0, 0);
                                } catch (IntentSender.SendIntentException e) {
                                    LogUtils.logD("ChangeThemeActivity", "downloadTheme: error", e.getMessage());
                                    ChangeThemeActivity.this.onDownloadThemFailed(e.getMessage());
                                    e.printStackTrace();
                                }
                        }
                    }
                }
            });
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    private void onDownloadThemSuccess() {
        LogUtils.logD("ChangeThemeActivity", "onDownloadThemSuccess: ");
        ToastUtils.show("restarting activity to apply change");
        isThemeModuleHasJustBeenDownloaded = true;
        Utils.mappingCurrentThemeToDynamic();
        SplitInstallHelper.updateAppInfo(getApplicationContext());
        new Handler().postDelayed(() -> {
            //Must restart activity after downloading theme
            startActivity(new Intent(this, ChangeThemeActivity.class));
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish();
        }, 300);
    }

    private void onDownloadThemFailed(String message) {
        ToastUtils.show("Failed");
    }

    public void handleOnClickSaveTheme(ThemeObj selectedTheme) {
        Utils.setCurrentTheme(selectedTheme);
        ChangeThemeActivity.requestToChangeTheme = true;
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
