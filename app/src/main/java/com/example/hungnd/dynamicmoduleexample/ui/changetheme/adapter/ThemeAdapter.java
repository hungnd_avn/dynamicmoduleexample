package com.example.hungnd.dynamicmoduleexample.ui.changetheme.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.hungnd.dynamicmoduleexample.R;
import com.example.hungnd.dynamicmoduleexample.Util.Utils;
import com.example.hungnd.dynamicmoduleexample.data.entity.ThemeObj;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ThemeAdapter extends RecyclerView.Adapter<ThemeAdapter.ViewHolder> {
    public ThemeObj selectedTheme;
    private int selectedPosition = 0;
    private List<ThemeObj> items = new ArrayList<>();

    public ThemeAdapter(ThemeObj themeObj) {
        selectedTheme = new ThemeObj(themeObj);
    }

    public ThemeAdapter() {

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_theme, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bindData(i);
    }

    @Override
    public int getItemCount() {
        if (items != null) {
            return items.size();
        } else {
            return 0;
        }
    }

    public void updateData(List<ThemeObj> themeObjs) {
        items = themeObjs;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.img_theme)
        ImageView imgTheme;
        @BindView(R.id.ll_selected_layout)
        RelativeLayout llSelectedLayout;

        public int position;
        private ThemeObj themeObj;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void bindData(int i) {
            this.position = i;
            themeObj = items.get(i);
            imgTheme.setImageResource(Utils.getThemeResourceId(themeObj));
            if (themeObj.fileName.equals(selectedTheme.fileName)) {
                llSelectedLayout.setVisibility(View.VISIBLE);
                selectedPosition = position;
            } else {
                llSelectedLayout.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View v) {
            selectedTheme = new ThemeObj(themeObj);
            notifyItemChanged(selectedPosition);
            notifyItemChanged(position);
        }
    }
}
