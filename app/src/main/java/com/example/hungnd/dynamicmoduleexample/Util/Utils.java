package com.example.hungnd.dynamicmoduleexample.Util;

import android.content.Context;

import com.example.hungnd.dynamicmoduleexample.common.BaseApplication;
import com.example.hungnd.dynamicmoduleexample.common.Constants;
import com.example.hungnd.dynamicmoduleexample.data.entity.ThemeObj;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class Utils {
    public static int getDrawableIdByName(String name) {
        Context context = BaseApplication.getInstance();
        return context.getResources().getIdentifier(
                name, "drawable", context.getPackageName());
    }

    public static int getDrawableIdFromModuleTheme(String fileName) {
        try {
            Class<?> cls = Class.forName("com.theme.DynamicUtils");
            Method method = cls.getMethod("getDrawableResourceId", String.class);
            int result = (int) method.invoke(null, fileName);
            LogUtils.logD("Utils", "getDrawableIdFromModuleTheme: ", result);
            return result;
        } catch (Exception e) {
            LogUtils.logD("Utils", "getDrawableIdFromModuleTheme: error", e.getMessage());
            e.printStackTrace();
        }
        return 0;
    }

    public static ThemeObj getCurrentTheme() {
        ThemeObj themeObj = Paper.book().read(Constants.KEY_CURRENT_THEME, null);
        if (themeObj == null) {
            themeObj = new ThemeObj("theme", 0, false);
        }
        return themeObj;
    }

    public static void setCurrentTheme(ThemeObj theme) {
        Paper.book().write(Constants.KEY_CURRENT_THEME, theme);
    }

    public static int getCurrentThemeResourceId() {
        ThemeObj themeObj = getCurrentTheme();
        return getThemeResourceId(themeObj);
    }

    public static int getThemeResourceId(ThemeObj themeObj) {
        if (!themeObj.isDynamicTheme) {
            return getDrawableIdByName(themeObj.fileName);
        } else {
            return getDrawableIdFromModuleTheme(themeObj.fileName);
        }
    }

    public static List<ThemeObj> getListDefaultThemes() {
        List<ThemeObj> themeObjs = new ArrayList<>();
        themeObjs.add(new ThemeObj("theme", 0, false));
        return themeObjs;
    }

    public static List<ThemeObj> getListDynamicThemes() {
        List<ThemeObj> themeObjs = new ArrayList<>();
        int TOTAL_THEME = 58;
        for (int i = 0; i < TOTAL_THEME; i++) {
            themeObjs.add(new ThemeObj("theme", i, true));
        }
        return themeObjs;
    }

    public static void mappingCurrentThemeToDynamic() {
        ThemeObj currentTheme = getCurrentTheme();
        currentTheme.isDynamicTheme = true;
//        currentTheme.position = ... change position if needed
//        currentTheme.category = ... change category if needed
        setCurrentTheme(currentTheme);
    }

}
