package com.example.hungnd.dynamicmoduleexample.ui.changetheme;

import com.example.hungnd.dynamicmoduleexample.Util.Utils;

public class DefaultThemeFragment extends ThemeFragment {
    @Override
    public void initData() {
        super.initData();
        adapter.updateData(Utils.getListDefaultThemes());
    }
}
