package com.example.hungnd.dynamicmoduleexample.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.hungnd.dynamicmoduleexample.R;
import com.example.hungnd.dynamicmoduleexample.Util.Utils;
import com.example.hungnd.dynamicmoduleexample.ui.changetheme.ChangeThemeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.img_background)
    ImageView imgBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        updateTheme();
    }

    public void onClickChangeThemeButton(View view) {
        startActivityForResult(new Intent(this, ChangeThemeActivity.class), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (ChangeThemeActivity.requestToChangeTheme) {
                //Must restart activity after downloading theme
                if (ChangeThemeActivity.isThemeModuleHasJustBeenDownloaded) {
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    finish();
                } else {
                    updateTheme();
                }
                ChangeThemeActivity.requestToChangeTheme = false;
                ChangeThemeActivity.isThemeModuleHasJustBeenDownloaded = false;
            }
        }
    }

    private void updateTheme() {
        imgBackground.setImageResource(Utils.getCurrentThemeResourceId());
    }
}
