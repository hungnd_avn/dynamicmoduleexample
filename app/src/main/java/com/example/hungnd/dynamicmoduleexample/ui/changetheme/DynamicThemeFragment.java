package com.example.hungnd.dynamicmoduleexample.ui.changetheme;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.example.hungnd.dynamicmoduleexample.Util.Utils;

public class DynamicThemeFragment extends ThemeFragment {
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnGetMoreTheme.setVisibility(View.GONE);
    }

    @Override
    public void initData() {
        super.initData();
        adapter.updateData(Utils.getListDynamicThemes());
    }
}
