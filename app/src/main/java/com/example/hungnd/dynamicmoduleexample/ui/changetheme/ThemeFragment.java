package com.example.hungnd.dynamicmoduleexample.ui.changetheme;


import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.hungnd.dynamicmoduleexample.R;
import com.example.hungnd.dynamicmoduleexample.Util.Utils;
import com.example.hungnd.dynamicmoduleexample.ui.changetheme.adapter.ThemeAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ThemeFragment extends Fragment {
    @BindView(R.id.recycler_view)
    public RecyclerView recyclerView;
    Unbinder unbinder;
    public ThemeAdapter adapter;
    @BindView(R.id.btn_get_more_theme)
    public Button btnGetMoreTheme;

    public ThemeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_theme, container, false);
        unbinder = ButterKnife.bind(this, view);
        initData();
        return view;
    }

    @CallSuper
    public void initData() {
        adapter = new ThemeAdapter(Utils.getCurrentTheme());
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(adapter);
    }

    @OnClick({R.id.btn_get_more_theme, R.id.btn_apply})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_get_more_theme:
                ((ChangeThemeActivity)getActivity()).startDownloadThemeProcess();
                break;
            case R.id.btn_apply:
                ((ChangeThemeActivity) getActivity()).handleOnClickSaveTheme(adapter.selectedTheme);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
