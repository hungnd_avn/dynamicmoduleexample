package com.example.hungnd.dynamicmoduleexample.common;

import android.app.Application;

import com.google.android.play.core.splitcompat.SplitCompatApplication;

import io.paperdb.Paper;

public class BaseApplication extends SplitCompatApplication {
    private static BaseApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Paper.init(this);
    }

    public static BaseApplication getInstance() {
        return instance;
    }
}
